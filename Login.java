public class Login {
    private User[] userArray;

    public Login(User[] inUserArray){
        this.userArray = inUserArray;
    }

    public boolean validate(String username, String password, boolean admin){
        boolean result = false;
        if(admin==false){
            for(User user : this.userArray){
                if(user.getUsername().equals(username) && user.getPassword().equals(password)){
                    result = true;
                }
                else{
                    System.out.println("huh");
                    result = false;
                }
            }
        }
        else if(admin==true){
            for(User user : this.userArray){
                if(user.getUsername().equals(username) && user.getPassword().equals(password) && user.getType()==UserType.ADMIN){
                    result = true;
                }
                else{
                    result = false;
                }
            }
        }
        return result;
    }
}

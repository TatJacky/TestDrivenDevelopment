import java.util.Random;
import java.lang.StringBuilder;

public class PasswordGenerator {
    private Random rand;
    private char letter;
    private int length;
    private String password;
    private StringBuilder str;

    public PasswordGenerator(int length){
        this.rand = new Random();
        this.length = length;
        this.str = new StringBuilder();
    }

    public void generateWeakPassword(){
        for(int i = 0; i < this.length; i++){
            this.letter = (char) ('a' + rand.nextInt(26));
            this.str.append(this.letter);
        }
        this.password = this.str.toString();
    }

    public void generateStrongPassword(){
        for(int i = 0; i < this.length*2; i++){
            int option = rand.nextInt(3);
            if(option == 0){
                this.letter = (char) ('a' + rand.nextInt(26));
                this.str.append(this.letter);
            }
            else if(option == 1){
                this.letter = (char) ('A' + rand.nextInt(26));
                this.str.append(this.letter);
            }
            else if(option == 2){
                this.str.append(rand.nextInt(10));
            }
        }
        this.password = this.str.toString();
    }


    public String getPassword(){
        return this.password;
    }

    public int getLength(){
        return this.length;
    }
}

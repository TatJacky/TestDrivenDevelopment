public class User {
    private String username;
    private String password;
    private UserType type;

    public User(String inUsername, String inPassword, UserType inType){
        this.username = inUsername;
        this.password = inPassword;
        this.type = inType;
    }

    public String getUsername() {
        return username;
    }
    public String getPassword() {
        return password;
    }
    public UserType getType() {
        return type;
    }
}

public class TDD{
    public static void main(String[] args){
        User[] userArray = new User[2];
        userArray[0] = new User("user", "123", UserType.REGULAR);
        userArray[1] = new User("admin", "789", UserType.ADMIN);

        Login L = new Login(userArray);
        System.out.println(L.validate("admin", "789", false));
        
        PasswordGenerator p = new PasswordGenerator(12);
        p.generateStrongPassword();
        System.out.println(p.getPassword());
        
    }
}

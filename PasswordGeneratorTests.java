import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class PasswordGeneratorTests {

    @Test
    public void testConstructor(){
        PasswordGenerator p = new PasswordGenerator(12);
        assertEquals(12, p.getLength());
    }

    @Test
    public void testPasswordLength(){
        PasswordGenerator p = new PasswordGenerator(8);
        p.generateWeakPassword();
        assertEquals(8, p.getLength());
    }

    @Test
    public void testStrongPasswordLength(){
        PasswordGenerator p = new PasswordGenerator(5);
        p.generateStrongPassword();
        assertEquals(10, p.getPassword().length());
    }
}
